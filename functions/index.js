const functions = require('firebase-functions');
const fetch = require('node-fetch')
// Create and Deploy Your First Cloud Functions
// https://firebase.google.com/docs/functions/write-firebase-functions

const line = require('@line/bot-sdk')
const config = {
    channelAccessToken: 'LHFyC4b4sVerEMHRwFdYS3stxKRz6DrBJvoIFHLQkVXOOXA0VoHDz0MgbdivllzyRFaArXpVaUC+TWVyoPYtovWdbxRL2x5gSZY3DBr56gq5tskBmGcc1T8XJFGSA+/IT2LF09W0udYtoMEETnp+JQdB04t89/1O/w1cDnyilFU=',
    channelSecret: 'cbd0f434efb601d8883f53f060a8ee65'
}

const get = (url) => {
    const options = {method: 'GET', headers: {'Content-Type': 'application/json', timeout: 5 * 1000}}
    return fetch(url, options).then(response => response.json())
}
exports.line_webhook = functions.https.onRequest((req, res) => {
    const client = new line.Client(config)
    var mappingNames = {
        'ศูนย์การเรียนรู้ฯ (3E)': {
            id: '4'
        },
        'สถานีตรวจวัดคุณภาพอากาศ CCDC': {
            id: '6'
        },
        'มหาวิทยาลัยเทคโนโลยีราชมงคลล้านนา': {
            id: '7'
        },
        'มหาวิทยาลัยแม่ฟ้าหลวง': {
            id: '9'
        },
        'BTS สะพานควาย': {
            id: '8'
        },
        'สำนักงานคณะกรรมการสภาวิจัยแห่งชาติ': {
            id: '10'
        }
    }
    if (req.method === 'POST') {
        const body = Object.assign({}, req.body)
        body.events.forEach((event) => {
            if (event.type === 'message' && event.message.type === 'text') {
                if (mappingNames[event.message.text] !== undefined) {
                    get(`https://www.cmuccdc.org/assets/api/chart/getAQIValue.php?s=${mappingNames[event.message.text].id}&type=pm25`).then(res => {
                        console.log(res)
                        const imageData = {
                            "type": "image",
                            "originalContentUrl": "https://www.cmuccdc.org/template/img/Air%20quality-01.png",
                            "previewImageUrl": "https://www.cmuccdc.org/template/img/Air%20quality-01.png"
                        }

                        const data = {
                            'type': 'text',
                            'text': res.report
                        }

                        if (res.report_type === 'unhealthy') {
                            imageData.originalContentUrl = 'https://www.cmuccdc.org/template/img/ccdc-03-en.png'
                            imageData.previewImageUrl = 'https://www.cmuccdc.org/template/img/ccdc-03-en.png'
                        }

                        client.replyMessage(event.replyToken, [imageData, data]).catch((e) => {
                            console.log(e)
                        })

                        return res
                    }).catch(e => {
                        console.log('error => ', e)
                    })
                }

                if (event.message.text.indexOf('@DustBoy') !== -1) {
                    console.log('sending replyMessage')

                    const data = {
                        'type': 'text',
                        'text': 'ยินดีรับใช้คร๊าบบบ กรุณาเลือกสถาที่ที่ใกล้ท่าน'
                    }
                    client.replyMessage(event.replyToken, [data]).catch((e) => {
                        console.log(e)
                    })
                    // const data = {
                    //     "type": "template",
                    //     "altText": "this is a carousel template",
                    //     "template": {
                    //         "type": "carousel",
                    //         "columns": [
                    //             {
                    //                 "thumbnailImageUrl": "https://www.cmuccdc.org/template/img/Air%20quality-03.png",
                    //                 "imageBackgroundColor": "#FFFFFF",
                    //                 "title": "this is menu",
                    //                 "text": "description",
                    //                 "defaultAction": {
                    //                     "type": "uri",
                    //                     "label": "View detail",
                    //                     "uri": "http://example.com/page/123"
                    //                 },
                    //                 "actions": [
                    //                     {
                    //                         "type": "postback",
                    //                         "label": "Buy",
                    //                         "data": "action=buy&itemid=111"
                    //                     },
                    //                     {
                    //                         "type": "postback",
                    //                         "label": "Add to cart",
                    //                         "data": "action=add&itemid=111"
                    //                     },
                    //                     {
                    //                         "type": "uri",
                    //                         "label": "View detail",
                    //                         "uri": "http://example.com/page/111"
                    //                     }
                    //                 ]
                    //             },
                    //             {
                    //                 "thumbnailImageUrl": "https://www.cmuccdc.org/template/img/Air%20quality-02.png",
                    //                 "imageBackgroundColor": "#000000",
                    //                 "title": "this is menu",
                    //                 "text": "description",
                    //                 "defaultAction": {
                    //                     "type": "uri",
                    //                     "label": "View detail",
                    //                     "uri": "http://example.com/page/222"
                    //                 },
                    //                 "actions": [
                    //                     {
                    //                         "type": "postback",
                    //                         "label": "Buy",
                    //                         "data": "action=buy&itemid=222"
                    //                     },
                    //                     {
                    //                         "type": "postback",
                    //                         "label": "Add to cart",
                    //                         "data": "action=add&itemid=222"
                    //                     },
                    //                     {
                    //                         "type": "uri",
                    //                         "label": "View detail",
                    //                         "uri": "http://example.com/page/222"
                    //                     }
                    //                 ]
                    //             }
                    //         ],
                    //         "imageAspectRatio": "rectangle",
                    //         "imageSize": "cover"
                    //     }
                    // }
                    // client.replyMessage(event.replyToken, data)
                }
                else if (event.message.text.indexOf('@lily') !== -1) {
                    const data = {
                        "type": "imagemap",
                        "baseUrl": "https://via.placeholder.com/1040x1040",
                        "altText": "This is an imagemap",
                        "baseSize": {
                            "height": 1040,
                            "width": 1040
                        },
                        "actions": [
                            {
                                "type": "uri",
                                "linkUri": "https://example.com/",
                                "area": {
                                    "x": 0,
                                    "y": 0,
                                    "width": 520,
                                    "height": 1040
                                }
                            },
                            {
                                "type": "message",
                                "text": "Hello",
                                "area": {
                                    "x": 520,
                                    "y": 0,
                                    "width": 520,
                                    "height": 1040
                                }
                            }
                        ]
                    }
                    client.replyMessage(event.replyToken, data)
                }
            }
        })
        res.status(200).send('post ok')
    }
    else if (req.method === 'GET') {
        res.status(200).send('get ok')
    }
    else {
        res.status(500).send('Forbidden!')
    }
})